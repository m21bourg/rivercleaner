-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 24 oct. 2023 à 16:49
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `rivercleaner`
--

-- --------------------------------------------------------

--
-- Structure de la table `havetool`
--

CREATE TABLE `havetool` (
  `Id_user` bigint(20) NOT NULL,
  `Id_tool` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `measurement`
--

CREATE TABLE `measurement` (
  `Id_measurement` bigint(20) NOT NULL,
  `Id_tool` bigint(20) NOT NULL,
  `State` text NOT NULL,
  `Weight` text NOT NULL,
  `Full` tinyint(1) NOT NULL,
  `Water_level` bigint(20) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tool`
--

CREATE TABLE `tool` (
  `Id_tool` bigint(20) NOT NULL,
  `localisation` text NOT NULL,
  `Name` text NOT NULL,
  `Lora_address` text NOT NULL,
  `River_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `Id_user` bigint(20) NOT NULL,
  `First_name` text NOT NULL,
  `Name` text NOT NULL,
  `Password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `waterlevel`
--

CREATE TABLE `waterlevel` (
  `id_Level` bigint(20) NOT NULL,
  `Level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `havetool`
--
ALTER TABLE `havetool`
  ADD UNIQUE KEY `Id_user` (`Id_user`,`Id_tool`),
  ADD KEY `Id_tool` (`Id_tool`);

--
-- Index pour la table `measurement`
--
ALTER TABLE `measurement`
  ADD PRIMARY KEY (`Id_measurement`),
  ADD KEY `Id_tool` (`Id_tool`),
  ADD KEY `Water_level` (`Water_level`);

--
-- Index pour la table `tool`
--
ALTER TABLE `tool`
  ADD PRIMARY KEY (`Id_tool`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id_user`);

--
-- Index pour la table `waterlevel`
--
ALTER TABLE `waterlevel`
  ADD PRIMARY KEY (`id_Level`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `measurement`
--
ALTER TABLE `measurement`
  MODIFY `Id_measurement` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tool`
--
ALTER TABLE `tool`
  MODIFY `Id_tool` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `Id_user` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `waterlevel`
--
ALTER TABLE `waterlevel`
  MODIFY `id_Level` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `havetool`
--
ALTER TABLE `havetool`
  ADD CONSTRAINT `havetool_ibfk_1` FOREIGN KEY (`Id_user`) REFERENCES `user` (`Id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `havetool_ibfk_2` FOREIGN KEY (`Id_tool`) REFERENCES `tool` (`Id_tool`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `measurement`
--
ALTER TABLE `measurement`
  ADD CONSTRAINT `measurement_ibfk_1` FOREIGN KEY (`Id_tool`) REFERENCES `tool` (`Id_tool`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `measurement_ibfk_2` FOREIGN KEY (`Water_level`) REFERENCES `waterlevel` (`id_Level`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
