#include <Arduino.h>
#include "lorae5.h"
#include "config_application.h"

#include "Ultrasonic.h" //Ultrasonic sensor
#include <Wire.h> //For I2C communication 

#include <stdarg.h> //for using 

/* TODO */
// Changer le nom de la classe LORAE5 et de l'objet instancié lorae5 sinon on s'y perd.
// Mettre une saisie au clavier 't' permettant de declancher un Uplink
// Mettre une tempo de 3s plutot qu'une boucle infinie lors du test de la présence de la liaison serie au début du programme.
// Faire une fonction capable de récupérer les data en downlink. 
// config_app : Mettre ADR à false et SF à 7 par default
// Pour la récupération des données en uplink, ainsi que l'envoi, il serait plus simple de travailler avec des tableaux uint8_t plutot que des String.
// Il faut privilégier les fonctions de la class String pour étudier le contenu des chaines de caractère lors du dialogue avec le module.
// Donner l'info que d'autres downlink sont pending... si c'est le cas. A voir si on active nous même un nouveau uplink dans le cas ou il reste des downlinks sur le NS.
// Dans la méthode checkResponse, il faut vérfier uniquement le mot et non pas \r\n, c'est plus simple pour la suite. De plus, on pourra se limiter à une partie de la réponse uniquement au lieu de mettre la totalité.
// Il faut solutionner l'erreur qui apparait en ABP.
// Mettre de une doc des fonctions utilisée en entête de chaque fonction
// Mettre la librairie dans le répertoire des librairies arduino.


LoraE5 LoRaE5(devEUI, appEUI, appKey, devAddr, nwkSKey, appSKey); 
bool USE_LORA =  true;


//----------------------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------WATER LEVEL GLOBAL VAR---------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------//
//Water leve is on the I2C Right/Droite
unsigned int low_data[8] = {0};
unsigned int high_data[12] = {0};


#define NO_TOUCH       0xFE
#define THRESHOLD      100
#define ATTINY1_HIGH_ADDR   0x78
#define ATTINY2_LOW_ADDR   0x77

//----------------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------ULTRASONIC +  BUZZER GLOBAL VAR-----------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------//
#define BUZZER 6 //Digital 6 -> Buzzer
#define RANGERPIN 7 //Digital 3 -> Ultrason


Ultrasonic ultrasonic(RANGERPIN);

void setup() {
  if(USE_LORA == true){
    LoRa_Serial.begin(9600);
    USB_Serial.begin(9600);
    while (!USB_Serial);
    USB_Serial.println("\r\n\n\n\n\n\n\n\n");
    USB_Serial.println("#######################################");
    USB_Serial.println("#### LoRaWAN Training Session #########");
    USB_Serial.println("#### Savoie Mont Blanc University #####\r\n");
    
    while(!LoRaE5.checkBoard());

    LoRaE5.setup(ACTIVATION_MODE, CLASS, SPREADING_FACTOR, ADAPTIVE_DR, CONFIRMED, PORT);
    LoRaE5.printInfo(SEND_BY_PUSH_BUTTON, FRAME_DELAY, LOW_POWER);
    //LoRaE5.getDevEUI();

    if(ACTIVATION_MODE == OTAA){
      LoRaE5.setDevEUI(devEUI);
      LoRaE5.setAppEUI(appEUI);
      LoRaE5.setAppKey(appKey);
      USB_Serial.println("\r\nJoin Procedure in progress...");
      while(LoRaE5.join() == false);
      delay(3000);
    }
    if(ACTIVATION_MODE == ABP){
      LoRaE5.setDevAddr(devAddr);
      LoRaE5.setNwkSKey(nwkSKey);
      LoRaE5.setAppSKey(appSKey);
    }else{
      Serial.begin(9600);
    }
  }
  //----------------------------------------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------BUZZER + WATER LEVEL----------------------------------------------------//
  //----------------------------------------------------------------------------------------------------------------------------------//
  pinMode(BUZZER, OUTPUT); // PIN Mode buzzer

  Wire.begin(); //INIT Water level sensor
}


//----------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------LOOP------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------//

void loop() {
  //local var for sensors
  long distance;
  int waterPercentage;
  

  distance = ultrasonicMesauerment(); //Do measurement of distance
  waterPercentage = check(); // get the water level of the river
  waterPercentage = 0 - waterPercentage;

  
  if(distance < 3.0){
    waterPercentage -= 4096;
  }
  String str = String(waterPercentage, HEX);

  Serial.print("Numerical value : ");
  Serial.println(waterPercentage);
  Serial.print("Binary Value: ");
  Serial.println(waterPercentage, BIN); // Print binary value

  Serial.print("Playload Hex format : ");
  Serial.println(str);
  //LoRaE5.sendMsg(_STRING, str);
  if(USE_LORA == true){
    LoRaE5.sendMsg(_HEXA, str);
    delay(FRAME_DELAY-10000);
  }
}



//----------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------EXTERNAL LOOP---------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------//

/*function ultrasonic measurement
* Make a distance mesurement in centimeter
* Set buzzer à High level when distance is low
*/
long ultrasonicMesauerment(){
  //Ultrasonic reader
  long distance;
  distance = ultrasonic.MeasureInCentimeters();
  delay(5);
  Serial.print("Distance ");
  Serial.println(distance);
  if (distance <= 3.0){
    digitalWrite(BUZZER, HIGH);
    Serial.println("Full System");
    delay(500);
    digitalWrite(BUZZER, LOW);
  }else{
    digitalWrite(BUZZER, LOW);
    Serial.println("System OK");
  }
  return distance;
}



/*
*
* Functions for WaterLevel
*
*/
void getHigh12SectionValue(void)
{
  memset(high_data, 0, sizeof(high_data));
  Wire.requestFrom(ATTINY1_HIGH_ADDR, 12);
  while (12 != Wire.available());

  for (int i = 0; i < 12; i++) {
    high_data[i] = Wire.read();
  }
  delay(10);
}

void getLow8SectionValue(void)
{
  memset(low_data, 0, sizeof(low_data));
  Wire.requestFrom(ATTINY2_LOW_ADDR, 8);
  while (8 != Wire.available());

  for (int i = 0; i < 8 ; i++) {
    low_data[i] = Wire.read(); // receive a byte as character
  }
  delay(10);
}


/*
*
* Function Check
* Return the percentage of water level
*
*/
int check()
{
  int sensorvalue_min = 250;
  int sensorvalue_max = 255;
  int low_count = 0;
  int high_count = 0;
  //while (1){
    uint32_t touch_val = 0;
    uint8_t trig_section = 0;
    low_count = 0;
    high_count = 0;
    getLow8SectionValue();
    getHigh12SectionValue();

    Serial.println("low 8 sections value = ");
    for (int i = 0; i < 8; i++)
    {
      Serial.print(low_data[i]);
      Serial.print(".");
      if (low_data[i] >= sensorvalue_min && low_data[i] <= sensorvalue_max)
      {
        low_count++;
      }
      if (low_count == 8)
      {
        Serial.print("      ");
        Serial.print("PASS");
      }
    }
    Serial.println("  ");
    Serial.println("  ");
    Serial.println("high 12 sections value = ");
    for (int i = 0; i < 12; i++)
    {
      Serial.print(high_data[i]);
      Serial.print(".");

      if (high_data[i] >= sensorvalue_min && high_data[i] <= sensorvalue_max)
      {
        high_count++;
      }
      if (high_count == 12)
      {
        Serial.print("      ");
        Serial.print("PASS");
      }
    }

    Serial.println("  ");
    Serial.println("  ");

    for (int i = 0 ; i < 8; i++) {
      if (low_data[i] > THRESHOLD) {
        touch_val |= 1 << i;

      }
    }
    for (int i = 0 ; i < 12; i++) {
      if (high_data[i] > THRESHOLD) {
        touch_val |= (uint32_t)1 << (8 + i);
      }
    }

    while (touch_val & 0x01)
    {
      trig_section++;
      touch_val >>= 1;
    }
    Serial.print("water level = ");
    Serial.print(trig_section * 5);
    Serial.println("% ");
    Serial.println(" ");
    Serial.println("*********************************************************");
    delay(1000);
    return trig_section * 5;
  //}
}