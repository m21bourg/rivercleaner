
#define ACTIVATION_MODE     		OTAA // ou ABP
#define CLASS						        CLASS_A
#define SPREADING_FACTOR    		7
#define ADAPTIVE_DR         		true
#define CONFIRMED           		false
#define PORT                		15

#define SEND_BY_PUSH_BUTTON 		false
#define FRAME_DELAY         		10000
//#define PAYLOAD_HELLO				    true
//#define PAYLOAD_TEMPERATURE    	false
//#define CAYENNE_LPP_         		false
#define LOW_POWER           		false

String devEUI = "70B3D57ED00630C5";

// Configuration for ABP Activation Mode
String devAddr = "00000000";
String nwkSKey = "00000000000000000000000000000000";
String appSKey = "00000000000000000000000000000000";

// Configuration for OTAA Activation Mode
String appKey = "FFC756C21917F54E3993167F8E68AB22"; 
String appEUI = "0000000000000000";
