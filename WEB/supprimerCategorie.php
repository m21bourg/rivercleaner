<!DOCTYPE html>
<html>
  <head>
    <script src="http://releases.flowplayer.org/js/flowplayer-3.2.12.min.js"></script>
      <?php
      include("function.php");
      bootstrap();
      ?>
  <!-- Un peu de style pour la visualisation -->
  <style type="text/css">
    .col-lg-8 { line-height: 200px; }
    .col-lg-12 { line-height: 80px; }
  </style>
  <title>La page d'accueil</title>
  </head>
    <body>
      <div class="container">
        <?php
          monheader();
          manav2();
        ?>
        <section class="jumbotron text-align">
          <div class="container">
            <h1 class="jumbotron-heading text-center">JeuxVideo.rt.com</h1>
            <strong class="text-center">Bienvenue sur le site officiel du jeux vidéo des R&T</strong>
            <form <?php  echo 'action = "'.$_SERVER["PHP_SELF"].'"' ?> method="POST">
              <div class="container">
                <div class="row">
                  <div class="col-sm-4">
                    <label for="cat">Sélectionnez une catégorie :</label>
                  </div>
                  <div class="col-sm-8">
                    <select id="cat" name="cat">
                      <?php afficherCategories(); ?>
                    </select>
                  </div>
                </div>
              </div>
              <input type="submit" name="submit">
            </form>
            <?php
              if (isset($_POST['submit'])) {
                supprimerCategorie($_POST);
              }
            ?>
          </div>
        </section>
      </div>
      <?php
      monfooter();

      function supprimerCategorie($_PST){
        $cat = $_PST['cat'];
                $sql = "DELETE FROM categoriejeux WHERE NomCategorie='".$cat."'";
                $conn = connexionBDD();
                if (mysqli_query($conn, $sql)) {
                  echo "La catégorie : ".$cat." à été supprimée avec succes";
                } else {
                  echo "Probleme ors de la suppression: " . mysqli_error($conn);
                }
      }
      ?>
  </body>
</html>