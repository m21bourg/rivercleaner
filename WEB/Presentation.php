<!DOCTYPE html>
<html>
  <head>
    <?php
    include("function.php");
    bootstrap();
    ?>

  <!-- Un peu de style pour la visualisation -->
  <style type="text/css">
    .col-lg-8 { line-height: 200px; }
    .col-lg-12 { line-height: 80px; }
  </style>
  <title>La page d'accueil</title>
  </head>
    <body>
     <div class="container">

            <?php
            monheader();
            manav2();
            ?>
                  <section class="jumbotron text-align">
                  <div class="container">
                    <h1 class="jumbotron-heading text-center">Developpeur</h1>
                          <div class="card mb-3" style="max-width: 700px;">
                                <div class="row no-gutters">
                                  <div class="col-md-4">
                                    <img src="Mathieu.jpg" class="card-img" alt="mathieu.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <div class="card-body">
                                      <h5 class="card-title">Mathieu Bourges</h5>
                                      <p class="card-text">06.95.91.09.01
                                          <br> mthbourges@gmail.com
                                          <br>
                                          <br>Meilleur dev ever ;)
                                      </p>
                                      <p class="card-text"><small class="text-muted">44 La Buzardiere 35340 Liffré</small></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                </section>

    </div>
    <?php
    monfooter();
    ?>
  </body>
</html>