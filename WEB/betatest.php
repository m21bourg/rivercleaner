<!DOCTYPE html>
<html>
  <head>
    <script src="http://releases.flowplayer.org/js/flowplayer-3.2.12.min.js"></script>
      <?php
      include("function.php");
      bootstrap();
      ?>
  <!-- Un peu de style pour la visualisation -->
  <style type="text/css">
    .col-lg-8 { line-height: 200px; }
    .col-lg-12 { line-height: 80px; }
  </style>
  <title>La page d'accueil</title>
  </head>
    <body>
      <div class="container">
        <?php
          monheader();
          manav2();
          $prix = 0;
          $titre = 'vide';
          $cat = 'inexistant';
          $contenue = 'vide';
        ?>
        <section class="jumbotron text-align">
          <div class="container">
            <h1 class="jumbotron-heading text-center">JeuxVideo.rt.com</h1>
            <strong class="text-center">Bienvenue sur le site officiel du jeux vidéo des R&T</strong>
            <form <?php echo 'action = "'.$_SERVER["PHP_SELF"].'"'; ?> method="POST"> 
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">
                    <label for="titre">Titre : </label>
                  </div>
                  <div class="col-sm-10">
                  <input type="text" name="titre" <?php echo 'value="'.$titre.'"'; ?> required >
                  </div> 
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">
                    <label for="prix">Prix : </label>
                  </div>
                  <div class="col-sm-10">
                    <input type="number" name="prix" <?php echo 'value="'.$prix.'"'; ?> >
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">
                    <label>Cetegorie : </label>
                  </div>
                  <div class="col-sm-10">
                    <select id="cat" name="cat">
                    <?php afficherCategories(); ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">
                    <label for="contenue">Contenus : </label>
                  </div>
                  <div class="col-sm-10"> 
                    <textarea name="conte" id="conte" rows="10" cols="80" required <?php echo 'value="'.$contenue.'"'; ?> ></textarea>
                  </div>
                </div>
              </div>
              <center><input type="submit" name="sibmit2"></center>
            </form>
          </div>
        </section>
      </div>
      <?php
      monfooter();
      ?>
  </body>
</html>