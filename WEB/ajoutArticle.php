<!DOCTYPE html>
<html>
  <head>
    <script src="http://releases.flowplayer.org/js/flowplayer-3.2.12.min.js"></script>
      <?php
      include("function.php");
      bootstrap();
      ?>
  <!-- Un peu de style pour la visualisation -->
  <style type="text/css">
    .col-lg-8 { line-height: 200px; }
    .col-lg-12 { line-height: 80px; }
  </style>
  <title>La page d'accueil</title>
  </head>
    <body>
          <div class="container">

            <?php
            monheader();
            manav2();
            ?>
                  <section class="jumbotron text-align">
                  <div class="container">
                    <h1 class="jumbotron-heading text-center">JeuxVideo.rt.com</h1>
                    <strong class="text-center">Bienvenue sur le site officiel du jeux vidéo des R&T</strong>
                    <form <?php echo 'action="'.$_SERVER["PHP_SELF"].'"'; ?> method="POST" enctype="multipart/form-data">
                      <label for="title">Titre Article :</label><br>
                      <input type="text" id="title" name="titre" required><br>
                      <label for="synopsis">Synopsis :</label><br>
                      <input type="text" id="synopsis" name="synopsis" required><br>
                      <label for="contenu">Contenu :</label><br>
                      <input type="text" id="contenu" name="contenu" required><br>
                      <label for="name">Nom du jeu :</label><br>
                      <input type="text" id="name" name="nom" required><br>
                      <label for="prix">Prix :</label><br>
                      <input type="number" id="prix" name="prix" required><br>
                      <label for="note">Note :</label><br>
                      <input type="number" id="note" name="note" required><br>
                      <label for="categorie">Catégorie du Jeu:</label><br> 
                      <select id="cat" name="cat">
                        <?php
                          $conn = connexionBDD();
                          if (!$conn) {
                            die("Connection failed: " . mysqli_connect_error());
                            echo "Vérifiez l'ID et le mdp dans fonction.php";
                          }
                          $query = "SELECT NomCategorie FROM categoriejeux";
                          $reponse = lectureBDD($query, $conn);
                          //print_r($reponse);
                          while($donnees = $reponse->fetch_object()){
                            foreach ($donnees as $key => $value) {
                              //echo $value.'<br>';
                              echo '<option value="'.$value.'">'.$value.'</option>';
                            }
                          }

                        ?>
                      </select><br>
                       <h2>Upload Fichier</h2>
                        <label for="fileUpload">Fichier:</label>
                        <input type="file" name="photo" id="fileUpload">
                        <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
                      <input type="hidden" name="date" <?php echo "value='".date('d/m/y')."'&agrave".date('H:i');?>><br>
                      <input type="submit" name="submit" value="Submit">

                    </form>
                    
                        <?php
                        $dir = scandir('./ImagesBD/');
                        //print_r($dir);
                        $nb = max($dir);
                        //echo $nb;
                        $nb++;
                        
                          //-----------------------------------------------------------------------------------------------------------
                          if (isset($_POST['submit'])) {
                           ajouterArticle($_POST);



                          }
                        ?>
                  </div>
                </section>
          </div>
    <?php
    monfooter();

    function ajouterArticle($_PST){
       $titre = $_PST['titre'];
                            $synopsis = $_PST['synopsis'];
                            $contenu = $_PST['contenu'];
                            $nom = $_PST['nom'];
                            $prix = $_PST['prix'];
                            $note = $_PST['note'];
                            $cat = $_PST['cat'];
                            $date = $_PST['date'];
                            //print_r($_PST);
                            //echo "<br>".$cat;

                            //------------------------------------RECUP ID categorie jeu---------------------------
                            $requete = 'SELECT Id_CategorieJeu FROM categoriejeux Where NomCategorie="'.$cat.'"';
                            $conn = connexionBDD();
                            if (!$conn) {
                              die("Connection failed: " . mysqli_connect_error());
                              echo "Vérifiez l'ID et le mdp dans function.php<br>";
                              echo "Si vous voyez ce message vous pouvez vous foutre de ma gueule...";
                            }
                            $reponse = lectureBDD($requete, $conn);
                            while($donnees = $reponse->fetch_object()){
                              foreach ($donnees as $key => $value) {
                                $cat = $value;
                              }
                            }
                            mysqli_close($conn);
                            
                            
                            //-----------------------------------------------INSERSION--------------------------------
                        $conn = connexionBDD();
                        // Check connection
                        if (!$conn) {
                          die("Connection failed: " . mysqli_connect_error());
                          echo "Vérifiez l'ID et le mdp dans fonction.php pour voir la ligne allez sur la page principale (ou cela s'affiche maintenant si j'ai bien fait mon job";
                        }

                        $sql = "INSERT INTO article(TitreArticle, ContenuArticle, Synopsis, NomJeu, PrixJeu, NoteJeu, DateCreationArticle, Id_CategorieJeu)
                        VALUES ('".$titre."','".$contenu."','".$synopsis."','".$nom."','".$prix."','".$note."','".$date."','".$cat."')";

                        if (mysqli_query($conn, $sql)) {
                          echo "<h1>Votre article à été ajouté avec succes</h1>";
                        } else {
                          echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                          echo "<br> Peut être que votre catégorie de jeu n'existe pas encore ...";
                          echo "<br><br>When I see you again ...";
                        }

                        
                        mysqli_close($conn);
                        //echo "Je pass ici";



                        //----------------------------------------------gestion upload photo--------------------
                        $dir = scandir('./ImagesBD/');
                        $nb = max($dir);
                        $nb++;
                            if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
                              //echo "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
                              $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                              $filename = $_FILES["photo"]["name"];
                              $filetype = $_FILES["photo"]["type"];
                              $filesize = $_FILES["photo"]["size"];

                              // Vérifie l'extension du fichier
                              $ext = pathinfo($filename, PATHINFO_EXTENSION);
                              if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");

                              // Vérifie la taille du fichier - 5Mo maximum
                              $maxsize = 5 * 1024 * 1024;
                              if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");
                              //---------------------------------------GESTION SQL-------------------------------------
                              
                              exec('mkdir '.$nb);
                              $conn = connexionBDD();
                              if (!$conn) {
                                die("Connection failed: " . mysqli_connect_error());
                                echo "Vérifiez l'ID et le mdp dans fonction.php pour voir la ligne allez sur la page principale (ou cela s'affiche maintenant si j'ai bien fait mon job";
                              }

                              $sql = 'INSERT INTO image(LienURL, Description) VALUES ("ImagesBD'.'/'.$nb.'/'.$nom.'","")';

                              if (mysqli_query($conn, $sql)) {
                                echo "<h1>La photo a été ajoutée avec succes</h1>";
                              } else {
                                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                                echo "<br> Peut être que votre catégorie de jeu n'existe pas encore ...";
                                echo "<br><br>When I see you again ...";
                              }
                              //---------------------------------------------------------------------------------------

                              // Vérifie le type MIME du fichier
                              if(in_array($filetype, $allowed)){
                                // Vérifie si le fichier existe avant de le télécharger.
                                if(file_exists("./upload/" . $_FILES["photo"]["name"])){
                                  echo $_FILES["photo"]["name"] . " existe déjà.";
                                } else{
                                  mkdir('./ImagesBD/'.$nb);
                                  //move_uploaded_file($_FILES["photo"]["tmp_name"], "./upload/" . $_FILES["photo"]["name"]);
                                  move_uploaded_file($_FILES["photo"]["tmp_name"], "./ImagesBD/".$nb."/" . $nom);
                                  //echo $_FILES["photo"]["name"];
                                  echo "Votre fichier a été téléchargé avec succès.";
                              } 
                              } else{
                                  echo "Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer."; 
                              }
                              } else{
                                  echo "Error: " . $_FILES["photo"]["error"];
                              }

                            //--------------------------------------------------------------------------------------
                        
                          
                          //-----------------------------------------------------------------------------------------------------------

    }
    ?>
  </body>
</html>