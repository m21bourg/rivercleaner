import paho.mqtt.client as mqtt
import json
import base64
from time import sleepe
import mysql.connector

dev_name = "YOUR_DEVICE_NAME"
app_id = "YOUR_APP_ID"
access_key = "YOUR_ACCESS_KEY"
username = "YOUR_USERNAME" #ex lora@ttn

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    #client.subscribe(app_id+"/devices/"+ dev_name + "/#")
    client.subscribe("#")

def on_message(client, userdata, msg):
    print("You get a message")
    messg = json.loads(msg.payload)
    print(messg)
    if messg["end_device_ids"]["device_id"] == dev_name :
        information = messg['uplink_message']['decoded_payload']
        print(information)
        if information['proximite'] != 0 :
            warterLevel = 256 - information['niveaudeau']
            fullSys = False
            state = "OK"
            if information['proximite'] == 0 :
                state = "MEASUREMENT FAIL"
                fullSys = False
            elif information['proximite'] != 255:
                fullSys = True
                
            if warterLevel <10 or warterLevel > 90 :
                state = "WARNING WL"
                
            #print("Water level : "+ str(warterLevel) + "  fullSystem : " + str(fullSys))
            #Change the query here
            query = "INSERT INTO measurement(Id_tool, State, Water_level, Full) VALUES( 1, '" +str(state) + "', "+ str(warterLevel) +", "+  str(fullSys) + ");"
            #print(query)
            execSQLRequest(query)
    
    print('Received: ' + str(messg['uplink_message']['decoded_payload']))
    
def execSQLRequest(query):
    #Change the nformation here
    db_connection = mysql.connector.connect(
        host='localhost',
        user='user',
        password='password',
        database='database'
    )
    cursor = db_connection.cursor()
    cursor.execute(query)
    db_connection.commit()
    print("QUERY EXECUTED")
    
    cursor.close()
    db_connection.close()
    
client = mqtt.Client()
client.username_pw_set(username=username, password=access_key)
client.on_connect = on_connect
client.on_message = on_message

client.connect("eu1.cloud.thethings.network", 1883, 60)
#client.loop_start()

# Start the MQTT loop
client.loop_forever()
